package mywork.demo.mapper;

import mywork.demo.pojo.MyUserPojo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface MyMapper {

    @Select("select password from user where username = #{username} ")
    public List<String> findUserByNameAndPassword(@Param("username")String username);

    @Select("select * from user ")
    public List<MyUserPojo> findAll();

}
