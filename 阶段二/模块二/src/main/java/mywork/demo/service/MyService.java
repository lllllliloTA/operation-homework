package mywork.demo.service;

import mywork.demo.dao.MyDao;
import mywork.demo.mapper.MyMapper;
import mywork.demo.pojo.MyUserPojo;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MyService {

    @Autowired
    private MyMapper myMapper;

    public String selectUser(String username,String password){
        List<String> userPasswordList = myMapper.findUserByNameAndPassword(username);

        List<MyUserPojo> all = myMapper.findAll();


        if (userPasswordList==null || userPasswordList.size()!=1){
            return "error";
        }else{
            String s = userPasswordList.get(0);
            if (password.equals(s)){
                return "ok";
            }else {
                return "error";
            }
        }

    }


}
