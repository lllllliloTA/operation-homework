package server;

import org.dom4j.*;
import org.dom4j.io.SAXReader;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;

/**
 * Minicat的主类
 */
public class Bootstrap {

    /**定义socket监听的端口号*/
    private int port = 8080;

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    private String webappsPath="";

    private Map<String,Map<String,Map<String,HttpServlet>>> hosts = new HashMap<>();
    /**
     * Minicat启动需要初始化展开的一些操作
     */
    public void start() throws Exception {

        // 加载解析相关的配置，web.xml
        loadServlet();


        // 定义一个线程池
        int corePoolSize = 10;
        int maximumPoolSize =50;
        long keepAliveTime = 100L;
        TimeUnit unit = TimeUnit.SECONDS;
        BlockingQueue<Runnable> workQueue = new ArrayBlockingQueue<>(50);
        ThreadFactory threadFactory = Executors.defaultThreadFactory();
        RejectedExecutionHandler handler = new ThreadPoolExecutor.AbortPolicy();


        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(
                corePoolSize,
                maximumPoolSize,
                keepAliveTime,
                unit,
                workQueue,
                threadFactory,
                handler
        );





        /*
            完成Minicat 1.0版本
            需求：浏览器请求http://localhost:8080,返回一个固定的字符串到页面"Hello Minicat!"
         */
        ServerSocket serverSocket = new ServerSocket(port);
        System.out.println("=====>>>Minicat start on port：" + port);

        /*while(true) {
            Socket socket = serverSocket.accept();
            // 有了socket，接收到请求，获取输出流
            OutputStream outputStream = socket.getOutputStream();
            String data = "Hello Minicat!";
            String responseText = HttpProtocolUtil.getHttpHeader200(data.getBytes().length) + data;
            outputStream.write(responseText.getBytes());
            socket.close();
        }*/


        /**
         * 完成Minicat 2.0版本
         * 需求：封装Request和Response对象，返回html静态资源文件
         */
        /*while(true) {
            Socket socket = serverSocket.accept();
            InputStream inputStream = socket.getInputStream();

            // 封装Request对象和Response对象
            Request request = new Request(inputStream);
            Response response = new Response(socket.getOutputStream());

            response.outputHtml(request.getUrl());
            socket.close();

        }*/


        /**
         * 完成Minicat 3.0版本
         * 需求：可以请求动态资源（Servlet）
         */
        /*while(true) {
            Socket socket = serverSocket.accept();
            InputStream inputStream = socket.getInputStream();

            // 封装Request对象和Response对象
            Request request = new Request(inputStream);
            Response response = new Response(socket.getOutputStream());

            // 静态资源处理
            if(servletMap.get(request.getUrl()) == null) {
                response.outputHtml(request.getUrl());
            }else{
                // 动态资源servlet请求
                HttpServlet httpServlet = servletMap.get(request.getUrl());
                httpServlet.service(request,response);
            }

            socket.close();

        }
*/

        /*
            多线程改造（不使用线程池）
         */
        /*while(true) {
            Socket socket = serverSocket.accept();
            RequestProcessor requestProcessor = new RequestProcessor(socket,servletMap);
            requestProcessor.start();
        }*/



        System.out.println("=========>>>>>>使用线程池进行多线程改造");
        /*
            多线程改造（使用线程池）
         */
        while(true) {

            Socket socket = serverSocket.accept();
            RequestProcessor requestProcessor = new RequestProcessor(socket,servletMap,hosts);
            //requestProcessor.start();
            threadPoolExecutor.execute(requestProcessor);
        }



    }


    private Map<String,HttpServlet> servletMap = new HashMap<String,HttpServlet>();

    /**
     * 加载解析web.xml，初始化Servlet
     */
    private void loadServlet() {
        InputStream resourceAsStream = this.getClass().getClassLoader().getResourceAsStream("server.xml");
        SAXReader saxReader = new SAXReader();
        try {
            Document serverDocument = saxReader.read(resourceAsStream);
            Element serverRootElement = serverDocument.getRootElement();
            List<Element> portNodes = serverRootElement.selectNodes("//Connect");
            Element serverelement = (Element) serverRootElement.selectSingleNode("//Connect");
            port = Integer.parseInt(serverelement.attributeValue("port"));

            List<Element> serverNodes = serverRootElement.selectNodes("//Host");
            //Host 信息封装
            for (int i = 0; i < serverNodes.size(); i++) {
                //准备解析 server.xml <Host appBase = "D:/webapps"> 的属性 appBase 路径下的工程
                //获取host 的两个属性值 name 与 appBase
                String name = serverNodes.get(i).attributeValue("name");
                webappsPath = serverNodes.get(i).attributeValue("appBase");
                webappsPath = webappsPath.replace("\\","/");
                //通过 appBase 的绝对路径 D:/webapps 扫描 该绝对路径下的工程
                File file = new File(webappsPath);
                if(file.exists() && !file.isFile()) {
                    File[] files = file.listFiles();
                    //Connect 信息封装
                    Map<String ,Map<String,HttpServlet>> Connect = new HashMap<>();
                    //获取到两个文件 ( 现有 demo1 与 demo2 )
                    for (File f : files) {
                        String path = f.getPath();
                        //获取 单个文件的文件名
                        String fileName = path.replace(file.getPath()+"\\","");
                        //约定文件下存在web.xml 执行-> 读取web.xml文件
                        File webFile = new File(path+"\\web.xml");
                        //web.xml解析
                        InputStream inputStream = new FileInputStream(webFile);
                        SAXReader webReader = new SAXReader();
                        try {
                            Document document = webReader.read(inputStream);
                            Element rootElement = document.getRootElement();

                            List<Element> selectNodes = rootElement.selectNodes("//servlet");

                            Map<String,HttpServlet> wrapper = new HashMap<>();
                            // warpper 信息封装
                            for (int j = 0; j < selectNodes.size(); j++) {
                                Element element =  selectNodes.get(j);

                                // <servlet-name>lagou</servlet-name>
                                Element servletnameElement = (Element) element.selectSingleNode("servlet-name");
                                String servletName = servletnameElement.getStringValue();
                                // <servlet-class>server.LagouServlet</servlet-class>
                                Element servletclassElement = (Element) element.selectSingleNode("servlet-class");
                                //获取servletClass的名字
                                String servletClass = servletclassElement.getStringValue();

                                // 根据servlet-name的值找到url-pattern
                                Element servletMapping = (Element) rootElement.selectSingleNode("/web-app/servlet-mapping[servlet-name='" + servletName + "']");
                                //传入 绝对路径 D:/webapps/demo1/
                                String pakageDir  = path.replace("\\","/");

                                MyClassLoader loader = new MyClassLoader(pakageDir+"/");
                                //自定义classLoader 进行加载servlet
                                Class<?> clazz  = loader.findClass(servletClass);

                                // /lagou
                                String urlPattern = servletMapping.selectSingleNode("url-pattern").getStringValue();
                                //封装进入wrapper
                                wrapper.put(urlPattern, (HttpServlet) clazz.newInstance());

                            }
                            //根据 localhost:port/demo1/servlet 规则进行封装
                            //现在已经封装了wrapper -> wrapper 为 /lagou
                            //准备封装的 Connect -> Connect 为 /demo1
                            //组合成 /demo1/lagou
                            Connect.put(fileName,wrapper);

                        } catch (DocumentException e) {
                            e.printStackTrace();
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        } catch (InstantiationException e) {
                            e.printStackTrace();
                        }
                        //准备封装的 Host -> Host 为 localhost
                        //组合成 localhost/demo1/lagou
                        hosts.put(name,Connect);
                    }

                }


            }

        }catch (Exception e){
            e.printStackTrace();
        }





        /*InputStream resourceAsStream = this.getClass().getClassLoader().getResourceAsStream("web.xml");
        SAXReader saxReader = new SAXReader();

        try {
            Document document = saxReader.read(resourceAsStream);
            Element rootElement = document.getRootElement();

            List<Element> selectNodes = rootElement.selectNodes("//servlet");
            for (int i = 0; i < selectNodes.size(); i++) {
                Element element =  selectNodes.get(i);
                // <servlet-name>lagou</servlet-name>
                Element servletnameElement = (Element) element.selectSingleNode("servlet-name");
                String servletName = servletnameElement.getStringValue();
                // <servlet-class>server.LagouServlet</servlet-class>
                Element servletclassElement = (Element) element.selectSingleNode("servlet-class");
                String servletClass = servletclassElement.getStringValue();


                // 根据servlet-name的值找到url-pattern
                Element servletMapping = (Element) rootElement.selectSingleNode("/web-app/servlet-mapping[servlet-name='" + servletName + "']");
                // /lagou
                String urlPattern = servletMapping.selectSingleNode("url-pattern").getStringValue();
                servletMap.put(urlPattern, (HttpServlet) Class.forName(servletClass).newInstance());

            }



        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }*/

    }


    /**
     * Minicat 的程序启动入口
     * @param args
     */
    public static void main(String[] args) {
        Bootstrap bootstrap = new Bootstrap();
        try {
            // 启动Minicat
            bootstrap.start();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
