package server;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class MyClassLoader extends ClassLoader {

    private String classDir;// 文件目录

    @Override
    public Class<?> findClass(String name) {
        //拼接 class文件的真实路径
        String realPath = classDir + name.replace(".", "/") + ".class";
        byte[] cLassBytes = null;
        Path path = null;

        try {

            path = Paths.get(new URI(realPath));
            cLassBytes = Files.readAllBytes(path);

        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
        }
        //调用父类的defineClass方法
        Class clazz = defineClass(name, cLassBytes, 0, cLassBytes.length);
        return clazz;
    }


    public MyClassLoader(String classDir) {
        this.classDir = "file:/".concat(classDir);//拼接 “file:/”前缀
    }






}
