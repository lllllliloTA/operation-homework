package server;

import java.io.InputStream;
import java.net.Socket;
import java.util.Map;

public class RequestProcessor extends Thread {

    private Socket socket;

    private Map<String,Map<String,Map<String,HttpServlet>>> host ;

    public RequestProcessor(Socket socket, Map<String, HttpServlet> servletMap, Map<String, Map<String, Map<String, HttpServlet>>> host) {
        this.socket = socket;
        this.host = host;
    }

    @Override
    public void run() {
        try{
            InputStream inputStream = socket.getInputStream();

            // 封装Request对象和Response对象
            Request request = new Request(inputStream);
            Response response = new Response(socket.getOutputStream());


            Map<String, Map<String, HttpServlet>> connect = host.get(request.getHost());

            if (connect == null || connect.size()==0){
                response.outputHtml("404");
                return;
            }

            String url = request.getUrl();
            String webName = url.split("/")[1];
            Map<String, HttpServlet> wrappers = connect.get(webName);

            if (wrappers == null || wrappers.size()==0){
                response.outputHtml("404");
                return;
            }
            HttpServlet httpServlet = wrappers.get(url.replace("/"+url.split("/")[1],""));



            // 静态资源处理
            if(httpServlet == null) {
                response.outputHtml(request.getUrl());
            }else{
                // 动态资源servlet请求
                System.out.println("动态资源servlet请求");
                httpServlet.service(request,response);
            }

            socket.close();

        }catch (Exception e) {
            e.printStackTrace();
        }

    }
}
