作业步骤:
1. 创建server.xml 并配置server.xml
2. 创建电脑磁盘下的webapps 并且 将demo1 和 demo2加入
3. 解析server.xml -> 将配置<Connect> <Host> 属性中的值拿出来
4. 通过Host 属性 appBase 获取 绝对路径 -> 找到 demo1 和 demo2 下面的web.xml 进行读取
5. 读取完之后 编写MyClassLoader 进行类加载
6. 类加载完之后把 信息封装
7. 在请求时 调用前进行判断 是否满足封装  的信息