package com.lagou.rpc.provider.utils;

import com.lagou.rpc.pojo.ServerNodeMsg;
import org.I0Itec.zkclient.ZkClient;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ZookeeperClient {

    private static ZkClient client ;

    private static String path ;

    private static List<String> serverPathList = new ArrayList<>();

    public List<String> getServerPath() {
        return serverPathList;
    }

    public ZkClient getClient() {
        return client;
    }

    public String getPath() {
        return path;
    }

    public static ZookeeperClient build(String path){
        client = new ZkClient(path);
        return new ZookeeperClient();
    }

    public void createNode(String serverPath, ServerNodeMsg data){
        serverPathList.add(serverPath);
        if (!client.exists(serverPath)){
            client.createEphemeral(serverPath,data);
        }
    }
    
    public void deleteNode(){
        for (String s : serverPathList) {
            client.deleteRecursive(s);
        }
    }



    /*public void listenTimes(String serverPathTimes){
        client.subscribeChildChanges(serverPathTimes, new IZkChildListener() {
            @Override
            public void handleChildChange(String s, List<String> list) throws Exception {
                Thread.sleep(5000);
                conTime(serverPathTimes);
            }
        });
    }*/
}
