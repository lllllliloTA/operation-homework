package com.lagou.rpc.provider;

import com.lagou.rpc.pojo.ServerNodeMsg;
import com.lagou.rpc.provider.server.RpcServer;
import com.lagou.rpc.provider.utils.ZookeeperClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServerBootstrapApplication implements CommandLineRunner {


    @Autowired
    RpcServer rpcServer;

    @Autowired
    private ZookeeperClient zookeeperClient;

    public static void main(String[] args) {
        SpringApplication.run(ServerBootstrapApplication.class, args);


    }

    @Override
    public void run(String... args) throws Exception {
        new Thread(new Runnable() {
            @Override
            public void run() {

                ServerNodeMsg serverNodeMsg = new ServerNodeMsg();
                serverNodeMsg.setIp("127.0.0.1");
                serverNodeMsg.setPort(8898);

                //创建zookeeper连接 并创建节点
                zookeeperClient.build("127.0.0.1:2181").createNode("/server/server01",serverNodeMsg);

                rpcServer.startServer("127.0.0.1", 8898);


            }
        }).start();

    }


}
