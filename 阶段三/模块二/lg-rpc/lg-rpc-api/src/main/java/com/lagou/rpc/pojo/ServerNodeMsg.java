package com.lagou.rpc.pojo;

import java.io.Serializable;

public class ServerNodeMsg implements Serializable {

    private String ip;
    private int port;
    private long times;

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public long getTimes() {
        return times;
    }

    public void setTimes(long times) {
        this.times = times;
    }
}
