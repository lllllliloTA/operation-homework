package com.lagou.rpc.consumer.utils;

import com.lagou.rpc.pojo.ServerNodeMsg;
import org.I0Itec.zkclient.ZkClient;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class MyZkClient {
    private static ZkClient client ;

    public static ZkClient getClient() {
        return client;
    }

    private List<ServerNodeMsg> serverPath;

    private static MyZkClient myZkClient = new MyZkClient();

    private MyZkClient() {

    }

    public List<ServerNodeMsg> getServerPath() {
        return serverPath;
    }

    public void setServerPath(List<ServerNodeMsg> serverPath) {
        this.serverPath = serverPath;
    }

    public static MyZkClient build(String path){
        if (client==null){
            client = new ZkClient(path);
        }
        return myZkClient;
    }

    public List getChild(){

        List<String> children = client.getChildren("/server");
        return children;

    }

    public Object readData(String path){
        return client.readData(path);
    }

    public void deleteNode(String path){
        client.delete(path);
    }

}
