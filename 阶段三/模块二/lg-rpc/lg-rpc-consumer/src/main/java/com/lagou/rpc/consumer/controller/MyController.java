package com.lagou.rpc.consumer.controller;

import com.lagou.rpc.api.IUserService;
import com.lagou.rpc.consumer.proxy.RpcClientProxy;
import com.lagou.rpc.consumer.utils.MyZkClient;
import com.lagou.rpc.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@RequestMapping("my")
public class MyController {

    /*public static void main(String[] args) {
            IUserService userService = (IUserService) RpcClientProxy.createProxy(IUserService.class);
            User user = userService.getById(1);
            System.out.println(user);
        }*/
    private IUserService userService;

    @Autowired
    private MyZkClient myZkClient;

    @RequestMapping("selectId")
    public void selectById() throws InterruptedException {

        while (true) {
            userService = (IUserService) RpcClientProxy.createProxy(IUserService.class);
            User user = userService.getById(1);
            System.out.println(user);
            Thread.sleep(5000);
        }

    }


    @RequestMapping("deleteId")
    public String deleteById(){
        userService = (IUserService) RpcClientProxy.createProxy(IUserService.class);
        int k = userService.deleteById(1);
        if (k==0){
            System.out.println("删除失败");
        }else {
            System.out.println("删除成功");
        }
        return "1";
    }

    public String updateById(){
        return null;
    }

}
