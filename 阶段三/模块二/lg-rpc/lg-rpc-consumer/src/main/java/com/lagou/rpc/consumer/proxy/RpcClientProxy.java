package com.lagou.rpc.consumer.proxy;

import com.alibaba.fastjson.JSON;
import com.lagou.rpc.common.RpcRequest;
import com.lagou.rpc.common.RpcResponse;
import com.lagou.rpc.consumer.client.RpcClient;
import com.lagou.rpc.consumer.utils.RpcClientCon;
import com.lagou.rpc.pojo.ServerNodeMsg;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;

/**
 * 客户端代理类-创建代理对象
 * 1.封装request请求对象
 * 2.创建RpcClient对象
 * 3.发送消息
 * 4.返回结果
 */
public class RpcClientProxy {

    public static Object createProxy(Class serviceClass) {
        return Proxy.newProxyInstance(Thread.currentThread().getContextClassLoader(),
                new Class[]{serviceClass}, new InvocationHandler() {

                    private List<RpcClient> myRpcClient = RpcClientCon.getCon();

                    @Override
                    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                        //1.封装request请求对象

                        RpcRequest rpcRequest = new RpcRequest();
                        rpcRequest.setRequestId(UUID.randomUUID().toString());
                        rpcRequest.setClassName(method.getDeclaringClass().getName());
                        rpcRequest.setMethodName(method.getName());
                        rpcRequest.setParameterTypes(method.getParameterTypes());
                        rpcRequest.setParameters(args);


                        RpcClient rpcClient = RpcClientCon.getRpcClient();

                        try {
                            long timeInMillis = Calendar.getInstance().getTimeInMillis();
                            //3.发送消息
                            Object responseMsg = rpcClient.send(JSON.toJSONString(rpcRequest));
                            RpcResponse rpcResponse = JSON.parseObject(responseMsg.toString(), RpcResponse.class);
                            if (rpcResponse.getError() != null) {
                                throw new RuntimeException(rpcResponse.getError());
                            }
                            //4.返回响应时间
                            long result = (long) rpcResponse.getResult();
                            String str = RpcClientCon.getStrServerPath();



                            if (result - timeInMillis > 5000){
                                //上报删除 当前zookeeper节点
                                RpcClientCon.deletNode(str);
                            }else {
                                //更新响应时间
                                ServerNodeMsg msg = RpcClientCon.getMsg(str);
                                msg.setTimes(result-timeInMillis);
                                RpcClientCon.conTime(str+"_times",msg);
                            }

                            return JSON.parseObject("="+result+"=", method.getReturnType());
                        } catch (Exception e) {
                            throw e;
                        } finally {
                            rpcClient.close();
                        }

                    }
                });
    }
}
