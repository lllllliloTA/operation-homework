package com.lagou.rpc.consumer;

import com.lagou.rpc.consumer.utils.MyZkClient;
import com.lagou.rpc.consumer.utils.RpcClientCon;
import com.lagou.rpc.pojo.ServerNodeMsg;
import lombok.SneakyThrows;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;
import java.util.List;

/**
 * SpringBoot启动类
 */
@SpringBootApplication
public class ClientBootStrapApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(ClientBootStrapApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        new Thread(new Runnable() {
            @SneakyThrows
            @Override
            public void run() {

                RpcClientCon.listData();
                RpcClientCon.listen();
                RpcClientCon.getCon();
            }
        }).start();
    }
}
