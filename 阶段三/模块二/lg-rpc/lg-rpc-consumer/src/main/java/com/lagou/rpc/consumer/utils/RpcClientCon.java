package com.lagou.rpc.consumer.utils;

import com.lagou.rpc.consumer.client.RpcClient;
import com.lagou.rpc.pojo.ServerNodeMsg;
import org.I0Itec.zkclient.IZkChildListener;
import org.I0Itec.zkclient.ZkClient;

import java.util.*;

public class RpcClientCon {

    private static MyZkClient zkClient = MyZkClient.build("127.0.0.1:2181");

    private static List<RpcClient> myRpcClient;
    //返回单个RpcClient
    private static RpcClient myRealRpcClient;

    private static String strServerPath;

    private static boolean flag = true;

    public static List<RpcClient> getCon(){
        if (flag){
            listData();
            myRpcClient = new ArrayList<>();
            List<ServerNodeMsg> serverPath = zkClient.getServerPath();

            for (ServerNodeMsg str : serverPath) {

                String ip = str.getIp();
                int port = str.getPort();
                //2.创建RpcClient对象
                RpcClient rpcClient = new RpcClient(ip,port);
                myRpcClient.add(rpcClient);

            }
            flag = false;
        }

        return myRpcClient;

    }

    public static String getStrServerPath() {
        return strServerPath;
    }

    /**
     * 负载均衡 调用
     * @return
     */
    public static RpcClient getRpcClient(){
        ZkClient client = MyZkClient.getClient();
        List<String> children = client.getChildren("/server");

        String str = null;
        long times = 0;
        for (String child : children) {
            // /server/server01

            String thisChild = "/server/"+child;

            ServerNodeMsg msg = getMsg(thisChild);

            long gTimes = msg.getTimes();

            if (times==0){
                gTimes = times;
            }
            if (gTimes<=times){
                times = gTimes;
                str = "/server/"+child;
            }
        }

        if (str!=null){
            // /server/server01
            ServerNodeMsg serverPath = getMsg(str);

            String ip = serverPath.getIp();
            int port = serverPath.getPort();
            //2.创建RpcClient对象
            myRealRpcClient = new RpcClient(ip,port);
            strServerPath = str;
        }

        return myRealRpcClient;
    }

    public static void conTime(String serverPath,ServerNodeMsg msg){
        String serverPathTimes = serverPath;
        ZkClient client = MyZkClient.getClient();

        if (client.exists(serverPathTimes)){
            client.writeData(serverPathTimes,msg);
        }else {
            client.createEphemeral(serverPathTimes,msg);
        }

    }

    public static ServerNodeMsg getMsg(String path){

        return (ServerNodeMsg)zkClient.readData(path);

    }

    public static void listen(){
        MyZkClient.getClient().subscribeChildChanges("/server", new IZkChildListener() {
            @Override
            public void handleChildChange(String s, List<String> list) throws Exception {
                flag = true;
                getCon();
            }
        });
    }

    public static void deletNode(String path){
        zkClient.deleteNode(path);
    }

    public static List<ServerNodeMsg> listData() {

        List<String> child = zkClient.getChild();
        List<ServerNodeMsg> data = new ArrayList<>();
        if (child.size()==0){

        }else {

            for (String str : child) {
                ServerNodeMsg serverPath = (ServerNodeMsg) zkClient.readData("/server/"+str);
                data.add(serverPath);
            }
            zkClient.setServerPath(data);
        }
        return data;
    }

}
