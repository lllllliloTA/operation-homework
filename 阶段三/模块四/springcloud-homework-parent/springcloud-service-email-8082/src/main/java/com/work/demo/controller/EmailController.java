package com.work.demo.controller;

import com.work.demo.email.SendEmail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/email")
public class EmailController {

    @Autowired
    private SendEmail sendEmail;

    @GetMapping("/sendEmail/{emailAddress}/{emailMsg}")
    public boolean sendEmail(@PathVariable("emailAddress")String emailAddress,@PathVariable("emailMsg")String emailMsg) throws Exception {
        return SendEmail.sendMail(emailAddress, emailMsg);
    }

}
