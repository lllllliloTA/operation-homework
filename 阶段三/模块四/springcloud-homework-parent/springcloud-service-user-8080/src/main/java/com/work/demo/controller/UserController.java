package com.work.demo.controller;

import com.work.demo.feign.CodeSendService;
import com.work.demo.feign.RedisUtils;
import com.work.demo.mapper.MyMapper;
import com.work.demo.pojo.MyUser;
import com.work.demo.utils.TokenProccessor;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseCookie;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private MyMapper myMapper;

    @Autowired
    private RedisUtils redisUtils;

    @Autowired
    private CodeSendService codeSendService;

    private boolean findUserExist(@PathVariable("email")String email){
        MyUser userExist = myMapper.findEmailExist(email);

        if (userExist!=null){
            return true;
        }
        return false;
    }

    /***
     * 登录接口
     * @param email
     * @param password
     * @param response
     * @return
     */
    @GetMapping("/loginByUser/{email}/{password}")
    public Boolean loginByUser(@PathVariable("email")String email, @PathVariable("password")String password, HttpServletResponse response){
        MyUser myUser = myMapper.confirmPasswordByUsername(email, password);
        if (myUser!=null){
            //登录流程
            //1. 获取一个token
            String token = TokenProccessor.getInstance().makeToken();
            //2.存入cookies中
            Cookie cookie = new Cookie("userToken", token);
            cookie.setPath("/");
            response.addCookie(cookie);

            //3.存入redis中
            redisUtils.setIntoRedis("userToken",token);
            return true;
        }
        return false;
    }

    /***
     * 注册接口
     * @param email
     * @param password
     * @param response
     * @return -1 用户已经存在 1 验证码校验不通过 0 注册成功
     */
    @GetMapping("/registerUser/{email}/{password}/{code}")
    public String registerUser(@PathVariable("email")String email,@PathVariable("password")String password,@PathVariable("code")String code,HttpServletResponse response){
        boolean userExist = findUserExist(email);
        if (userExist){
            return "-1";
        }
        boolean b = codeSendService.validateCode(email, code);
        //验证码校验不通过
        if (!b){
            return "1";
        }

        Integer integer = myMapper.registerUser(email, password);
        //登录流程
        //1. 获取一个token
        String token = TokenProccessor.getInstance().makeToken();
        //2.存入cookies中
        Cookie cookie = new Cookie("userToken", token);
        cookie.setPath("/");
        response.addCookie(cookie);

        return "0";
    }

}
