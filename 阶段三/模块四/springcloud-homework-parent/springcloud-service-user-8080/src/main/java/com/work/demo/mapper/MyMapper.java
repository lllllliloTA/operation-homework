package com.work.demo.mapper;


import com.work.demo.pojo.MyUser;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface MyMapper {

    /**
     *  登录查询邮箱与密码
     * */
    @Select("select * from user where email = #{email} and password = #{password}")
    public MyUser confirmPasswordByUsername(@Param("email")String email,@Param("password")String password);

    /**
     * 查询邮箱是否存在
     */
    @Select("select * from user where email = #{email} ")
    public MyUser findEmailExist(@Param("email")String email);

    /**
     * 用户注册方法
     * */
    @Insert("insert into user(email,password) values(#{email},#{password})")
    public Integer registerUser(@Param("email")String email,@Param("password")String password);

}
