package com.work.demo.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Component
@FeignClient(name="springcloud-service-code",path = "/code",contextId = "code")
public interface CodeSendService {

    /**
     * 验证 验证码是否是从邮件获取的验证码
     * */
    @GetMapping("/validate/{emailAddress}/{code}")
    public boolean validateCode(@PathVariable("emailAddress")String emailAddress, @PathVariable("code")String code);

}
