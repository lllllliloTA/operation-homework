package com.work.demo.utils;

import java.util.Random;

/*
 *  随机验证码类
 */
public class RandomCode {

    private static int numbers = 6;

    public static String getcode(){

        StringBuffer str = new StringBuffer();
        for (int i =0;i<numbers;i++){
            str.append(getRandom());
        }

        return str.toString();
    }

    private static Integer getRandom(){
        Random rand = new Random();
        int i = rand.nextInt(9);
        return i;
    }



}
