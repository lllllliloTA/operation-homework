package com.work.demo.redis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.concurrent.TimeUnit;

@Component
public class RedisUtils {

    @Autowired
    private RedisTemplate<String,Object> redisTemplate;

    public void setMsgCode(String address,String emailMsg){
        redisTemplate.opsForValue().set(address,emailMsg,300, TimeUnit.SECONDS);
    }

    public String getMsgCode(String address){
        String msg = (String) redisTemplate.opsForValue().get(address);
        return msg;
    }

    public void setBlackList(String ip){
        redisTemplate.boundListOps("blackList").leftPush(ip);
    }

    public List<Object> getBlackList(){
        Long blackListSize = redisTemplate.boundListOps("blackList").size();
        List<Object> blackList = redisTemplate.boundListOps("blackList").range(0, blackListSize);
        return blackList;
    }

}
