package com.work.demo.service.Impl;

import com.work.demo.service.EmailService;

public class EmailServiceFallback implements EmailService {
    @Override
    public boolean sendEmail(String emailAddress, String emailMsg) throws Exception {
        return false;
    }
}
