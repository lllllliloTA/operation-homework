package com.work.demo.controller;

import com.work.demo.redis.RedisUtils;
import com.work.demo.service.EmailService;
import com.work.demo.utils.RandomCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/code")
public class SendEmailController {

    @Autowired
    private EmailService emailService;

    @Autowired
    private RedisUtils redisUtils;
    /**
     * 通过邮件获取验证码
     * */
    @GetMapping("/codeSendEmail/{emailAddress}")
    public boolean sendEmail(@PathVariable("emailAddress")String emailAddress) throws Exception {
        String getcode = null;
        //先判断是否 300s内是否 获取过验证码 -> redis 超时时间300s
        String msgCode = redisUtils.getMsgCode(emailAddress);
        if (msgCode==null){
            getcode = RandomCode.getcode();
        }else {
            getcode = msgCode;
        }

        boolean isflag = emailService.sendEmail(emailAddress, getcode);

        if (isflag){
            //进入这里的时候代表邮件发送成功 添加email地址为key 验证码为value 的集合进入Redis缓存 默认设置300S 超时时间
            redisUtils.setMsgCode(emailAddress,getcode);
        }else {
            //邮件发送失败
            return false;
        }

        return true;
    }
    /**
     * 验证 验证码是否是从邮件获取的验证码
     * */
    @GetMapping("/validate/{emailAddress}/{code}")
    public boolean validateCode(@PathVariable("emailAddress")String emailAddress,@PathVariable("code")String code){
        String msgCode = redisUtils.getMsgCode(emailAddress);

        if (msgCode==null){
            return false;
        }else {
            if (code.equals(msgCode)){
                return true;
            }else {
                return false;
            }
        }

    }

}
