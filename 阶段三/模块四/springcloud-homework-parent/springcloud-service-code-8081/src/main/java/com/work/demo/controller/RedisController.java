package com.work.demo.controller;

import com.work.demo.redis.RedisUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/redis")
public class RedisController {

    @Autowired
    private RedisUtils redisUtils;

    @GetMapping("/setIntoRedis/{MessageKey}/{MessageValue}")
    public boolean setIntoRedis(@PathVariable("MessageKey")String MessageKey,@PathVariable("MessageValue")String MessageValue){
        redisUtils.setMsgCode(MessageKey,MessageValue);
        return true;
    }

    @GetMapping("/getRedisKey/{MessageKey}")
    public String getRedisKey(@PathVariable("MessageKey")String MessageKey){
        return  redisUtils.getMsgCode(MessageKey);
    }

    @GetMapping("/setBlackList/{ip}")
    public boolean setBlackList(@PathVariable("ip")String ip){
        redisUtils.setBlackList(ip);
        return true;
    }

    @GetMapping("/getBlackList")
    public List<Object> getBlackList(){
        return redisUtils.getBlackList();
    }


}
