package com.work.demo.service;

import com.work.demo.service.Impl.EmailServiceFallback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 *  Feign远程调用发送短信的方法
 */
@Service
@FeignClient(name = "springcloud-service-email",path = "/email")
public interface EmailService {

    @GetMapping("/sendEmail/{emailAddress}/{emailMsg}")
    public boolean sendEmail(@PathVariable("emailAddress")String emailAddress, @PathVariable("emailMsg")String emailMsg) throws Exception;

}
