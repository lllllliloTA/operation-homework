package com.home.demo.filter;

import com.home.demo.utils.RedisUtils;
import com.home.demo.utils.TokenProccessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpCookie;
import org.springframework.http.ResponseCookie;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

@Component
public class MyFilter implements GlobalFilter, Ordered {

    //请求次数
    private Integer times = 0;

    private Map<String,Integer> map = new HashMap<>();

    @Autowired
    private RedisUtils redisUtils;

    public MyFilter() {
        Executors.newSingleThreadScheduledExecutor().scheduleWithFixedDelay(new Runnable() {
            @Override
            public void run() {
                synchronized (times){
                    times = 0;
                }
            }
        }, 1, 60, TimeUnit.SECONDS);
    };


    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        times++;
        //从上下文中获取request对象
        ServerHttpRequest request = exchange.getRequest();
        ServerHttpResponse response = exchange.getResponse();
        //IP防暴刷过滤器
        String ip = request.getRemoteAddress().getHostString();
        List<Object> redisBlackList = getRedisBlackList();
        if (redisBlackList!=null || redisBlackList.size()>0){
            for (Object ipC : redisBlackList) {
                if(ip.equals(ipC.toString())){
                    String data = "您已经被加入黑名单中!!!!";
                    DataBuffer wrap = response.bufferFactory().wrap(data.getBytes(StandardCharsets.UTF_8));
                    return response.writeWith(Mono.just(wrap));
                }
            }
        }
        if(map.containsKey(ip)){
            Integer integer = map.get(ip);
            integer++;
            map.put(ip,integer);
        }else {
            map.put(ip,1);
        }
        //计数器 -> 1分钟内只允许100个请求访问 超出则返回error
        if(times>100){
            String data = "Request be denied!!!!";
            DataBuffer wrap = response.bufferFactory().wrap(data.getBytes(StandardCharsets.UTF_8));
            //当1分钟内请求次数超过100次 将请求次数最多的IP加入Redis黑名单中
            setRedisBlackList(ip);
            return response.writeWith(Mono.just(wrap));
        }
        //如果cookies.size() = 0 则 进入 注册或登录流程 不为0 则进入其他流程
        if(request.getURI().getPath().contains("login")||request.getURI().getPath().contains("register")){
            //4. 放行 执行原方法
            return chain.filter(exchange);
        }else {
            //未登录状态网关拦截
            MultiValueMap<String, HttpCookie> cookies = request.getCookies();
            if (cookies.size()==0||cookies.get("userToken")==null){
                redisUtils.getRedisKey("userToken");

                //没有登录 返回登录界面
                String data = "Request be denied!!!!";
                DataBuffer wrap = response.bufferFactory().wrap(data.getBytes(StandardCharsets.UTF_8));
                return response.writeWith(Mono.just(wrap));
            }

        }

        //合法请求放行
        return chain.filter(exchange);
    }

    @Override
    public int getOrder() {
        return 0;
    }

    private void setRedisBlackList(String ip){
        redisUtils.setBlackList(ip);
    }

    private List<Object> getRedisBlackList(){
         return redisUtils.getBlackList();
    }

}
