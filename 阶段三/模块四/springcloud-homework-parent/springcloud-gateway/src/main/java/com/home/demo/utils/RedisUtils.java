package com.home.demo.utils;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Component
@FeignClient(name = "springcloud-service-code",path = "/redis")
public interface RedisUtils {

    @GetMapping("/setIntoRedis/{MessageKey}/{MessageValue}")
    public boolean setIntoRedis(@PathVariable("MessageKey")String MessageKey, @PathVariable("MessageValue")String MessageValue);

    @GetMapping("/getRedisKey/{MessageKey}")
    public String getRedisKey(@PathVariable("MessageKey")String MessageKey);

    @GetMapping("/setBlackList/{ip}")
    public boolean setBlackList(@PathVariable("ip")String ip);

    @GetMapping("/getBlackList")
    public List<Object> getBlackList();


}
