package com.lagou.rpc.consumer.controller;

import com.lagou.rpc.api.IUserService;
import com.lagou.rpc.consumer.proxy.RpcClientProxy;
import com.lagou.rpc.pojo.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@RequestMapping("my")
public class MyController {

    /*public static void main(String[] args) {
            IUserService userService = (IUserService) RpcClientProxy.createProxy(IUserService.class);
            User user = userService.getById(1);
            System.out.println(user);
        }*/
    private IUserService userService;

    @RequestMapping("selectId")
    public String selectById(){
        userService = (IUserService) RpcClientProxy.createProxy(IUserService.class);
        User user = userService.getById(1);
        System.out.println(user);
        return "1";
    }


    @RequestMapping("deleteId")
    public String deleteById(){
        userService = (IUserService) RpcClientProxy.createProxy(IUserService.class);
        int k = userService.deleteById(1);
        if (k==0){
            System.out.println("删除失败");
        }else {
            System.out.println("删除成功");
        }
        return "1";
    }

    public String updateById(){
        return null;
    }

}
