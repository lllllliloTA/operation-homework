package com.lagou.rpc.consumer;

import com.lagou.rpc.api.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * SpringBoot启动类
 */
@SpringBootApplication
public class ClientBootStrapApplication {

    public static void main(String[] args) {
        SpringApplication.run(ClientBootStrapApplication.class, args);
    }

}
