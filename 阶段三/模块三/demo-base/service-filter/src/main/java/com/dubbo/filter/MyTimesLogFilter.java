package com.dubbo.filter;

import javafx.collections.transformation.SortedList;
import org.apache.dubbo.common.constants.CommonConstants;
import org.apache.dubbo.common.extension.Activate;
import org.apache.dubbo.rpc.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

//添加Activate注解 进行注册 并且使用group属性 来指定 是服务端还是消费端
@Activate(group = {CommonConstants.CONSUMER})
public class MyTimesLogFilter implements Filter {
    //输出日志
    private final static Logger LOGGER = LoggerFactory.getLogger(CommonConstants.class);

    private static Map<String,Integer> timesMap = new HashMap<>();

    private static ScheduledFuture<?> scheduled ;

    private static Map<String, LinkedList> mapLog ;

    private static LinkedList<Long> times1 = new LinkedList<>();
    private static LinkedList<Long> times2 = new LinkedList<>();
    private static LinkedList<Long> times3 = new LinkedList<>();

    public MyTimesLogFilter() throws InterruptedException {

        if (scheduled == null){
            scheduled = Executors.newSingleThreadScheduledExecutor().scheduleWithFixedDelay(new Runnable() {
                @Override
                public void run() {
                    LOGGER.info("我60秒执行一次");
                    //执行时循环获取锁对象 当获取到锁对象的时候 进行初始化操作
                    while (true){
                        synchronized(timesMap){
                            //60s执行一次 timesMap的初始化
                            timesMap = new HashMap<>();
                            mapLog = new HashMap<>();
                            times1 = new LinkedList<>();
                            times2 = new LinkedList<>();
                            times3 = new LinkedList<>();
                            break;
                        }
                    }

                }
            }, 1, 60, TimeUnit.SECONDS);
        }
        Thread.sleep(1000);
        startListen();

    }

    @Override
    public Result invoke(Invoker<?> invoker, Invocation invocation) throws RpcException {
        //获取开始时间
        long startTimes = System.currentTimeMillis();
        //获取每次调用的方法名
        String methodName = invocation.getMethodName();
        try {

            if (timesMap.containsKey(methodName)){
                synchronized(timesMap){
                    Integer integer = timesMap.get(methodName);
                    integer++;
                    timesMap.put(methodName,integer);
                }
            }else {
                synchronized(timesMap){
                    timesMap.put(methodName,1);
                }
            }
            //利用invoker对象 去执行 原对象类的方法
            return invoker.invoke(invocation);
        }finally {
            long endTimes = System.currentTimeMillis();
            synchronized(timesMap){
                switch (methodName){
                    case "sayHello":
                        startLog(methodName,times1);
                        times1.add(endTimes-startTimes);
                    case "sayHello2":
                        startLog(methodName,times2);
                        times2.add(endTimes-startTimes);
                    case "sayHello3":
                        startLog(methodName,times3);
                        times3.add(endTimes-startTimes);
                }
            }
        }

    }
    //开启时间日志
    private void startLog(String methodName,LinkedList list){
        if(mapLog==null){
            mapLog = new HashMap<>();
        }else {
            if (!mapLog.containsKey(methodName)) {
                mapLog.put(methodName, list);
            }
        }
    }

    public void startListen() {
        Executors.newSingleThreadScheduledExecutor().scheduleWithFixedDelay(new Runnable() {
            @Override
            public void run() {
                LOGGER.info("我5秒输出一次");
                //遍历timesMap
                synchronized (timesMap){
                    for (Map.Entry<String, Integer> maps : timesMap.entrySet() ){
                        String key = maps.getKey();
                        Integer value = maps.getValue();

                        if (mapLog.containsKey(key)){
                            LinkedList<Long> linkedList = mapLog.get(key);
                            linkedList.sort(Long::compareTo);
                            Long last = linkedList.getLast();
                            long lo = (long) (last*0.90);
                            int out = 0;
                            for (Long la : linkedList) {
                                if (la>lo){
                                    out++;
                                }
                            }
                            LOGGER.info("当前方法名为:{},请求数为:{},TP90请求数为{}",key,value,((double)(linkedList.size()-out)/(double) linkedList.size())*100+"%");
                        }
                        //LOGGER.info("当前方法名为:{},请求数为:{}",key,value);
                    }
                }

            }
        }, 1, 5, TimeUnit.SECONDS);
    }
}
