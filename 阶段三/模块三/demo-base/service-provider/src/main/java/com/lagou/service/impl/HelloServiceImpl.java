package com.lagou.service.impl;

import com.lagou.service.HelloService;
import org.apache.dubbo.config.annotation.Service;

import java.util.Random;

@Service
public class HelloServiceImpl   implements HelloService {

    private static int times = 100;

    private static int getRandom(){
        // 0<= random and random < n
        Random ran = new Random();
        int random = ran.nextInt(times);
        return random;
    }
    @Override
    public String sayHello(String name) {
        try {
            Thread.sleep(getRandom());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "hello1:"+name;
    }

    @Override
    public String sayHello2(String name) {
        try {
            Thread.sleep(getRandom());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "hello2:"+name;
    }

    @Override
    public String sayHello3(String name) {
        try {
            Thread.sleep(getRandom());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "hello3:"+name;
    }
}
