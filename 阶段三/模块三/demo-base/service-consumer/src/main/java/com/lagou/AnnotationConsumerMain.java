package com.lagou;

import com.lagou.bean.ConsumerComponent;
import com.lagou.utils.ThreadRunner;
import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;


public class AnnotationConsumerMain {
    public static void main(String[] args) throws IOException, InterruptedException {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ConsumerConfiguration.class);
        context.start();
        ConsumerComponent service = context.getBean(ConsumerComponent.class);
        //创建固定线程池
        ExecutorService executorService = Executors.newFixedThreadPool(3);
        for (int i =0;i<2500;i++){
            executorService.execute(new ThreadRunner(service));
        }
        //关闭线程池
        executorService.shutdown();
        //等待线程结束
        executorService.awaitTermination(3, TimeUnit.MINUTES);
    }

    @Configuration
    @PropertySource("classpath:/dubbo-consumer.properties")
    @ComponentScan("com.lagou.bean")
    @EnableDubbo
    static class ConsumerConfiguration {

    }
}
