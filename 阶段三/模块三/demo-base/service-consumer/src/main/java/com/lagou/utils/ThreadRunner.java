package com.lagou.utils;

import com.lagou.bean.ConsumerComponent;

import java.util.concurrent.ThreadLocalRandom;

public class ThreadRunner extends Thread{

    private static ConsumerComponent component;

    public ThreadRunner(ConsumerComponent component){
        this.component= component;
    }


    @Override
    public void run() {
        final int i = ThreadLocalRandom.current().nextInt(3);
        String result = null;
        switch (i) {
            case 0:
                result = component.sayHello( "i: "  +i );
                break;
            case 1:
                result = component.sayHello2("i: " +i );
                break;
            case 2:
                result = component.sayHello3("i: " +i );
                break;
            default:
                break;

        }


    }



}
