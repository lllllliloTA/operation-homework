package com.lagou.bean;

import com.lagou.service.HelloService;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Component;

@Component
public class ConsumerComponent {

    @Reference
    private HelloService helloService;

    public String sayHello(String name) {
        return helloService.sayHello(name);
    }

    public String sayHello2(String name){
        return helloService.sayHello2(name);
    }

    public String sayHello3(String name){
        return helloService.sayHello3(name);
    }
}
