package com.lagou.edu.factory;

import com.lagou.edu.annoUtils.*;
import org.reflections.Reflections;

import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedType;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class AnnoBeanFactory {

    private static Map<Object, Object> map = new HashMap<>();  // 存储对象

    static  {
        try {

            //通过工具类来扫描 com.lagou.edu下的包
            Reflections reflections = new Reflections("com.lagou.edu");
            //使用reflections.getTypesAnnotatedWith(AnnoService.class); 获取到被@AnnoService 注解的类的class对象
            //获取到 Types(类) 上的Anno(注解)名为 (注解类.class)
            Set<Class<?>> AnnoComponentSet = reflections.getTypesAnnotatedWith(AnnoComponent.class);
            Set<Class<?>> AnnoServiceSet = reflections.getTypesAnnotatedWith(AnnoService.class);
            Set<Class<?>> AnnoControllerSet = reflections.getTypesAnnotatedWith(AnnoController.class);
            AnnoComponentSet.addAll(AnnoServiceSet);
            AnnoComponentSet.addAll(AnnoControllerSet);
            //遍历 Set集合 将 里面的class对象 进行 创建 注入到简易IOC容器map中
            for (Class<?> aClass : AnnoComponentSet) {
                //创建 Bean 对象 并将Bean 放入map容器中
                String value = null;
                //尝试根据注解的不同获取 注解里面的值
                if(aClass.isAnnotationPresent(AnnoComponent.class)){
                    value = aClass.getAnnotation(AnnoComponent.class).value();
                }else if(aClass.isAnnotationPresent(AnnoService.class)){
                    value = aClass.getAnnotation(AnnoService.class).value();
                }else if(aClass.isAnnotationPresent(AnnoController.class)){
                    value = aClass.getAnnotation(AnnoController.class).value();
                }

                //实例化Class对象
                Object o = aClass.newInstance();
                //有value值的时候
                if (value!=null&&!"".equals(value)){
                    map.put(value,o);
                }
                //当实现接口的时候
                if (aClass.getInterfaces().length!= 0 ){
                    map.put(aClass.getInterfaces()[0],o);
                }
                //当前class类
                map.put(aClass,o);
            }

            //声明式事务 使用动态代理对象
            Set<Class<?>> AnnoTransactionalSet = reflections.getTypesAnnotatedWith(AnnoTransactional.class);

            for (Class<?> bClass : AnnoTransactionalSet) {
                String value = null;

                if (!bClass.isInterface()) {
                    if(bClass.isAnnotationPresent(AnnoComponent.class)){
                        value = bClass.getAnnotation(AnnoComponent.class).value();
                    }else if(bClass.isAnnotationPresent(AnnoService.class)){
                        value = bClass.getAnnotation(AnnoService.class).value();
                    }else if(bClass.isAnnotationPresent(AnnoController.class)){
                        value = bClass.getAnnotation(AnnoController.class).value();
                    }
                }
                ProxyFactory proxyFactory = (ProxyFactory) getMap(ProxyFactory.class);
                //有value值的时候
                if(value!=null && !"".equals(value)){
                    Object jdkProxy = proxyFactory.getJdkProxy(map.get(value));
                    map.put(value,jdkProxy);
                }
                //无value值的时候
                if(bClass.getInterfaces().length!= 0){
                    Class anInterface = bClass.getInterfaces()[0];
                    Object cglibProxy = proxyFactory.getCglibProxy(map.get(anInterface));
                    map.put(anInterface,cglibProxy);
                }else{
                    Object jdkProxy = proxyFactory.getJdkProxy(map.get(bClass));
                    map.put(bClass,jdkProxy);
                }
            }

            //注入依赖
            //获得当前类的所有声明字段
            for (Class<?> c : AnnoComponentSet){
                if (!c.isInterface()){
                    Object o = map.get(c);
                    for (Field field:c.getDeclaredFields()){
                        Annotation[] annotations = field.getAnnotations();
                        for (Annotation annotation : annotations) {
                            if (AnnoAutoWired.class == annotation.annotationType()) {
                                field.setAccessible(true);
                                field.set(o, map.get(field.getType()));
                            }
                        }
                    }
                }
            }
            System.out.println(1);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static Object getMap(Object key) {
        return map.get(key);
    }

    public static void clearMap(){
        map.clear();
    }
}