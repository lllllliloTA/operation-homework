package com.lagou.edu.listener;

import com.lagou.edu.factory.AnnoBeanFactory;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
//tomcat 初始化 加载监听器
//实现 ServletContextListener 接口 并且 在web.xml中配置 Listen标签
public class MyListener implements ServletContextListener {
    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        try {
            Class.forName("com.lagou.edu.factory.AnnoBeanFactory");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        AnnoBeanFactory.clearMap();
    }
}
