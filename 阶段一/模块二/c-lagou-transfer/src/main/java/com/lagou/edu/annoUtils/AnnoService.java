package com.lagou.edu.annoUtils;

import java.lang.annotation.*;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface AnnoService {
    //自定义Service层注解
    //仿照 @Component
    //定义默认值
    //用途 获取IOC容器中单例池中的key
    String value() default "";

}
