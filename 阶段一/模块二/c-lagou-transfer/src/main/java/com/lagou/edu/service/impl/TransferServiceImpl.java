package com.lagou.edu.service.impl;

import com.lagou.edu.annoUtils.AnnoAutoWired;
import com.lagou.edu.annoUtils.AnnoService;
import com.lagou.edu.annoUtils.AnnoTransactional;
import com.lagou.edu.dao.AccountDao;
import com.lagou.edu.pojo.Account;
import com.lagou.edu.service.TransferService;

@AnnoService("transferService")
@AnnoTransactional
public class TransferServiceImpl implements TransferService {

    @AnnoAutoWired
    private AccountDao accountDao;

    @Override
    public void transfer(String fromCardNo, String toCardNo, int money) throws Exception {

            Account from = accountDao.queryAccountByCardNo(fromCardNo);
            Account to = accountDao.queryAccountByCardNo(toCardNo);

            from.setMoney(from.getMoney()-money);
            to.setMoney(to.getMoney()+money);

            accountDao.updateAccountByCardNo(to);
            int t = 1/0;
            accountDao.updateAccountByCardNo(from);

    }
}
