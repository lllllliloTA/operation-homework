package com.lagou.edu.utils;

import com.lagou.edu.annoUtils.AnnoAutoWired;
import com.lagou.edu.annoUtils.AnnoComponent;

import java.sql.SQLException;

@AnnoComponent
public class TransactionManager {

    @AnnoAutoWired
    private ConnectionUtils connectionUtils;

    public void setConnectionUtils(ConnectionUtils connectionUtils) {
        this.connectionUtils = connectionUtils;
    }

    // 开启手动事务控制
    public void beginTransaction() throws SQLException {
        //使用的是JDBC设置事务是否自动提交
        //connectionUtils.getCurrentThreadConn() 获取的是Connection对象
        //将Connection对象中默认自动提交事务 setAutoCommit(true);
        //将Connection对象中的setAutoCommit设为false 则不会自动提交事务
        connectionUtils.getCurrentThreadConn().setAutoCommit(false);
    }


    // 提交事务
    public void commit() throws SQLException {
        //调用Connection对象的提交事务方法
        connectionUtils.getCurrentThreadConn().commit();
    }


    // 回滚事务
    public void rollback() throws SQLException {
        //调用Connection对象的回滚事务方法
        connectionUtils.getCurrentThreadConn().rollback();
    }
}
