package com.lagou.edu.servlet;

import com.lagou.edu.annoUtils.AnnoAutoWired;
import com.lagou.edu.annoUtils.AnnoComponent;
import com.lagou.edu.controller.MyController;
import com.lagou.edu.factory.AnnoBeanFactory;
import com.lagou.edu.utils.JsonUtils;
import com.lagou.edu.pojo.Result;
import com.lagou.edu.service.TransferService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name="transferServlet",urlPatterns = "/transferServlet")
public class TransferServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        MyController mapBean = (MyController) AnnoBeanFactory.getMap(req.getServletPath());

        mapBean.doPost(req, resp);

    }
}
