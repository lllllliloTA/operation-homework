package com.lagou.edu.annoUtils;

import java.lang.annotation.*;


@Documented
@Target({ElementType.CONSTRUCTOR, ElementType.METHOD, ElementType.PARAMETER, ElementType.FIELD, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface AnnoAutoWired {
    boolean required() default true;
}
