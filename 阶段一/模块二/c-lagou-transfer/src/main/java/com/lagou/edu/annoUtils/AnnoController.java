package com.lagou.edu.annoUtils;
import java.lang.annotation.*;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface AnnoController {

    String value() default "";

}
