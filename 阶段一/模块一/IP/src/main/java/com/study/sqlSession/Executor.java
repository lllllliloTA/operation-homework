package com.study.sqlSession;

import com.study.pojo.Configuration;
import com.study.pojo.MappedStatement;

import java.sql.SQLException;
import java.util.List;

public interface Executor {

    public <E> List<E> query(Configuration configuration, MappedStatement mappedStatement,Object... params) throws Exception;

    public Integer insertData(Configuration configuration, MappedStatement mappedStatement,Object... params) throws Exception;

    public Integer updateData(Configuration configuration, MappedStatement mappedStatement,Object... params) throws Exception;

    public Integer deleteData(Configuration configuration, MappedStatement mappedStatement,Object... params) throws Exception;

}
