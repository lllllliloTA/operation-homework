package com.study.sqlSession;

import com.study.config.XMLConfigBuilder;
import com.study.pojo.Configuration;
import org.dom4j.DocumentException;

import java.io.InputStream;

public class SqlSessionFactoryBuilder {
    public SqlSessionFactory builder(InputStream in) throws Exception {
        //第一件事 :使用dom4j 解析输入的字节流，将解析出来的内容封装到Configruation中
        XMLConfigBuilder xmlConfigBuilder = new XMLConfigBuilder();
        Configuration configuration = xmlConfigBuilder.parseConfiguration(in);
        //第二件事 : 创建SqlSessionFactory对象 工厂模式 生产sqlsession会话对象
        SqlSessionFactory sqlSessionFactory = new DefaultSqlSessionFactory(configuration);

        return sqlSessionFactory;
    }
}
