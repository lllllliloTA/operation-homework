package com.study.sqlSession;

import com.study.config.BoundSql;
import com.study.pojo.Configuration;
import com.study.pojo.MappedStatement;
import com.study.utils.GenericTokenParser;
import com.study.utils.ParameterMapping;
import com.study.utils.ParameterMappingTokenHandler;
import com.study.utils.TokenHandler;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class SimpleExecutor implements Executor{


    @Override
    public <E> List<E> query(Configuration configuration, MappedStatement mappedStatement, Object... params) throws Exception {
        //JDBC 代码编写
        //1.注册驱动,获取连接
        Connection connection = configuration.getDataSource().getConnection();
        //2.获取SQL语句
        String sql = mappedStatement.getSql();
        BoundSql boundSql = getBoundSql(sql);
        //3.获取到预处理对象
        PreparedStatement preparedStatement = connection.prepareStatement(boundSql.getSqlText());
        //4.设置参数
        List<ParameterMapping> mappingList = boundSql.getMappingList();
        //获取到了参数类的全路径
        String paramterType = mappedStatement.getParamterType();
        Class<?> paramterTypeClass = getClassType(paramterType);
        for (int i = 0; i < mappingList.size(); i++) {
            ParameterMapping parameterMapping = mappingList.get(i);
            String content = parameterMapping.getContent();
            //通过反射获取实体的属性
            Field field = paramterTypeClass.getDeclaredField(content);
            //设置暴力访问
            field.setAccessible(true);
            Object o = field.get(params[0]);

            preparedStatement.setObject(i+1,o);

        }
        //5.执行SQL
        ResultSet resultSet = preparedStatement.executeQuery();
        //6.封装返回结果集
        String resultType = mappedStatement.getResultType();
        Class<?> classType = getClassType(resultType);

        List<Object> objects = new ArrayList<>();
        while (resultSet.next()){
            ResultSetMetaData metaData = resultSet.getMetaData();
            Object o = classType.newInstance();
            for (int i = 1; i < metaData.getColumnCount()+1; i++) {
                //获取字段名
                String columnName = metaData.getColumnName(i);
                //获取字段的值
                Object object = resultSet.getObject(columnName);
                //使用反射或者内省,根据数据库表与实体类对应关系,进行封装
                PropertyDescriptor propertyDescriptor = new PropertyDescriptor(columnName, classType);
                Method writeMethod = propertyDescriptor.getWriteMethod();
                //将具体的值封装到参数中
                writeMethod.invoke(o,object);

            }
            objects.add(o);
        }
        return (List<E>) objects;
    }

    @Override
    public Integer insertData(Configuration configuration, MappedStatement mappedStatement, Object... params) throws Exception {
        //JDBC 代码编写
        //1.注册驱动,获取连接
        Connection connection = configuration.getDataSource().getConnection();
        //2.获取SQL语句
        String sql = mappedStatement.getSql();
        BoundSql boundSql = getBoundSql(sql);
        //3.获取到预处理对象
        PreparedStatement preparedStatement = connection.prepareStatement(boundSql.getSqlText());
        //4.设置参数
        List<ParameterMapping> mappingList = boundSql.getMappingList();
        //获取到了参数类的全路径
        String paramterType = mappedStatement.getParamterType();
        Class<?> paramterTypeClass = getClassType(paramterType);
        for (int i = 0; i < mappingList.size(); i++) {
            ParameterMapping parameterMapping = mappingList.get(i);
            String content = parameterMapping.getContent();
            //通过反射获取实体的属性
            Field field = paramterTypeClass.getDeclaredField(content);
            //设置暴力访问
            field.setAccessible(true);
            Object o = field.get(params[0]);

            preparedStatement.setObject(i+1,o);

        }
        //5.执行SQL
        int i = preparedStatement.executeUpdate();

        return i;
    }

    @Override
    public Integer updateData(Configuration configuration, MappedStatement mappedStatement, Object... params) throws Exception {
        //JDBC 代码编写
        //1.注册驱动,获取连接
        Connection connection = configuration.getDataSource().getConnection();
        //2.获取SQL语句
        String sql = mappedStatement.getSql();
        BoundSql boundSql = getBoundSql(sql);
        //3.获取到预处理对象
        PreparedStatement preparedStatement = connection.prepareStatement(boundSql.getSqlText());
        //4.设置参数
        List<ParameterMapping> mappingList = boundSql.getMappingList();
        //获取到了参数类的全路径
        String paramterType = mappedStatement.getParamterType();
        Class<?> paramterTypeClass = getClassType(paramterType);
        for (int i = 0; i < mappingList.size(); i++) {
            ParameterMapping parameterMapping = mappingList.get(i);
            String content = parameterMapping.getContent();
            //通过反射获取实体的属性
            Field field = paramterTypeClass.getDeclaredField(content);
            //设置暴力访问
            field.setAccessible(true);
            Object o = field.get(params[0]);

            preparedStatement.setObject(i+1,o);

        }
        //5.执行SQL
        int i = preparedStatement.executeUpdate();

        return i;
    }

    @Override
    public Integer deleteData(Configuration configuration, MappedStatement mappedStatement, Object... params) throws Exception {
        //JDBC 代码编写
        //1.注册驱动,获取连接
        Connection connection = configuration.getDataSource().getConnection();
        //2.获取SQL语句
        String sql = mappedStatement.getSql();
        BoundSql boundSql = getBoundSql(sql);
        //3.获取到预处理对象
        PreparedStatement preparedStatement = connection.prepareStatement(boundSql.getSqlText());
        //4.设置参数
        //批量删除?
        int result = 0;
        for (int i = 0;i<params.length;i++){
            Object param = params[i];
            Object setparam = param.getClass()==Integer.class? param : null ;
            preparedStatement.setObject(1,param);
            //5.执行SQL
            result = preparedStatement.executeUpdate();
        }


        return result;
    }

    private Class<?> getClassType(String paramterType) throws ClassNotFoundException {
        if (paramterType!=null){
            Class<?> aClass = Class.forName(paramterType);
            return aClass;
        }
        return null;
    }

    /**
     * 第一件事 :完成#{} 的解析
     * 第二件事 :解析出#{}的值进行存储
     * */
    private BoundSql getBoundSql(String sql){

        //标记处理类 ，配置标准解析器来完成对占位符解析的处理工作
        ParameterMappingTokenHandler parameterMappingTokenHandler = new ParameterMappingTokenHandler();
        GenericTokenParser genericTokenParser = new GenericTokenParser("#{", "}", parameterMappingTokenHandler);
        String parseSql = genericTokenParser.parse(sql);
        //解析出来的参数名称
        List<ParameterMapping> parameterMappings = parameterMappingTokenHandler.getParameterMappings();
        BoundSql boundSql = new BoundSql(parseSql,parameterMappings);

        return boundSql;
    }
}
