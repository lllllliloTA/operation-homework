package com.study.config;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import com.study.io.Resources;
import com.study.pojo.Configuration;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.beans.PropertyVetoException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;

public class XMLConfigBuilder {

    private Configuration configuration;

    public XMLConfigBuilder() {
        this.configuration = new Configuration();
    }

    //该方法使用dom4j将数据流进行解析，封装配置文件信息的方法
    public Configuration parseConfiguration(InputStream in) throws DocumentException, PropertyVetoException {

        Document document = new SAXReader().read(in);
        //获取配置文件中的跟对象<configuration>
        Element rootElement = document.getRootElement();
        List<Element> list = rootElement.selectNodes("//property");
        Properties properties = new Properties();
        for (Element element : list) {
            String name = element.attributeValue("name");
            String value = element.attributeValue("value");
            properties.setProperty(name,value);
        }
        //使用连接池，解决JDBC连接数据库的问题
        ComboPooledDataSource comboPooledDataSource = new ComboPooledDataSource();
        comboPooledDataSource.setDriverClass(properties.getProperty("driverClass"));
        comboPooledDataSource.setJdbcUrl(properties.getProperty("jdbcUrl"));
        comboPooledDataSource.setUser(properties.getProperty("username"));
        comboPooledDataSource.setPassword(properties.getProperty("password"));

        configuration.setDataSource(comboPooledDataSource);

        //mapper.xml解析:拿到mapper.xml的全路径然后进行dom4j解析
        List<Element> mapper = rootElement.selectNodes("mapper");

        for (Element element : mapper) {
            //获取mapper.xml的全路径
            String mapperPath = element.attributeValue("resource");
            //通过Resources写好的静态方法去加载mapper.xml的输入字节流
            InputStream resourceAsSteam = Resources.getResourceAsSteam(mapperPath);
            //使用dom4j解析
            XMLMapperBuilder xmlMapperBuilder = new XMLMapperBuilder(configuration);

            xmlMapperBuilder.parse(resourceAsSteam);
        }
        
        return  configuration;
    }
}
