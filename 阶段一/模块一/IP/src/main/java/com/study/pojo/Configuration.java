package com.study.pojo;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

public class Configuration {

    //配置文件的数据源信息
    private DataSource dataSource;
    /**
     * key: statementId 是mapper.xml的唯一的值
     * value : 就是封装好的MapperStatement对象
     * */
    private Map<String,MappedStatement> mappedStatementList = new HashMap<>();


    public DataSource getDataSource() {
        return dataSource;
    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public Map<String, MappedStatement> getMappedStatementList() {
        return mappedStatementList;
    }

    public void setMappedStatementList(Map<String, MappedStatement> mappedStatementList) {
        this.mappedStatementList = mappedStatementList;
    }
}
