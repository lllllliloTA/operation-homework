package com.study.sqlSession;

import com.study.pojo.Configuration;

public interface SqlSessionFactory {

    public SqlSession openSession();

}
