package com.study.sqlSession;

import com.study.pojo.Configuration;
import com.study.pojo.MappedStatement;

import java.lang.reflect.*;
import java.util.List;

public class DefaultSqlSession implements  SqlSession{

    private Configuration configuration;

    public DefaultSqlSession(Configuration configuration) {
        this.configuration = configuration;
    }

    @Override
    public <E> List<E> selectList(String statementId, Object... params) throws Exception {

        //将要完成对SimpleExecutor里的query的调用
        SimpleExecutor simpleExecutor = new SimpleExecutor();

        MappedStatement mappedStatement = configuration.getMappedStatementList().get(statementId);

        List<Object> queryList = simpleExecutor.query(configuration, mappedStatement, params);


        return (List<E>) queryList;
    }

    @Override
    public <T> T selectOne(String statementId, Object... params) throws Exception {
        List<Object> objects = selectList(statementId, params);
        if (objects.size()==1){
            return (T) objects.get(0);
        }else{
            throw new RuntimeException("查询结果出错");
        }

    }

    @Override
    public Integer insertUser(String statementId, Object... params) throws Exception{
        //将要完成对SimpleExecutor里的query的调用
        SimpleExecutor simpleExecutor = new SimpleExecutor();
        MappedStatement mappedStatement = configuration.getMappedStatementList().get(statementId);
        Integer integer = simpleExecutor.insertData(configuration, mappedStatement, params);
        return integer;
    }

    @Override
    public Integer updateUser(String statementId, Object... params) throws Exception {
        //将要完成对SimpleExecutor里的query的调用
        SimpleExecutor simpleExecutor = new SimpleExecutor();
        MappedStatement mappedStatement = configuration.getMappedStatementList().get(statementId);
        Integer integer = simpleExecutor.updateData(configuration, mappedStatement, params);
        return integer;
    }

    @Override
    public Integer deleteUser(String statementId, Object... params) throws Exception {
        //将要完成对SimpleExecutor里的query的调用
        SimpleExecutor simpleExecutor = new SimpleExecutor();
        MappedStatement mappedStatement = configuration.getMappedStatementList().get(statementId);
        Integer integer = simpleExecutor.deleteData(configuration, mappedStatement, params);
        return integer;
    }

    @Override
    public <T> T getMapper(Class<?> mapperClass) {
        //使用JDK动态代理来为Dao层接口生成代理对象并返回

        Object proxyInstance = Proxy.newProxyInstance(DefaultSqlSession.class.getClassLoader(), new Class[]{mapperClass}, new InvocationHandler() {
            @Override
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                //参数解析: proxy : 当前代理对象的引用
                //method : 当前被调用方法的引用 例子: 当前调用类是IPPR_TEST -> List<User> all = userDao.findAll();
                //当前调用方法就是 findAll()
                //args传递的参数 user
                //不管如何封装如何优化底层还是去执行JDBC代码
                //根据不同情况去调用selectOne 或者 selectList 去执行JDBC完成操作
                //第一步:准备参数
                //1.statementId 是SQL语句唯一标识 是由nameSpace+.+Id组成
                //由于无法获取statementId所以 要将namespace命名为接口全路径 ID命名为方法名
                //然后通过 method方法获取 接口名 与 类名 使得namespace.id = 接口全限定名.方法名
                //获取方法名 : findAll
                String methodName = method.getName();
                //获取该方法所在的类的字节码对象 .getName获取类名
                String className = method.getDeclaringClass().getName();
                String statementId = className+"."+methodName;
                //2.准备参数2 params : args
//                //获取被调用方法的 返回值类型
//                Type genericReturnType = method.getGenericReturnType();
//                //是否进行了泛型类型参数化
//                if (genericReturnType instanceof ParameterizedType){
//                    List<Object> objects = selectList(statementId, args);
//                    return objects;
//                }else{
//                    Object selectOne = selectOne(statementId, args);
//                    return selectOne;
//                }
                MappedStatement mappedStatement = configuration.getMappedStatementList().get(statementId);
                String id = mappedStatement.getId();
                if("findAll".equals(id)){
                    List<Object> objects = selectList(statementId, args);
                    return objects;
                }else if ("findByCondition".equals(id)){
                    Object selectOne = selectOne(statementId, args);
                    return selectOne;
                }else if("insertUser".equals(id)){
                    Integer integer = insertUser(statementId, args);
                    return integer;
                }else if("updateUser".equals(id)){
                    Integer integer = updateUser(statementId, args);
                    return integer;
                }else if("deleteUser".equals(id)){
                    Integer integer = deleteUser(statementId, args);
                    return integer;
                }

                return null;
            }
        });


        return (T) proxyInstance;
    }
}
