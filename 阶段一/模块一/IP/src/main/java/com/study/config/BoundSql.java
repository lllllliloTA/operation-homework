package com.study.config;

import com.study.utils.ParameterMapping;

import java.util.List;

public class BoundSql {
    private String SqlText;
    private List<ParameterMapping> mappingList;

    public BoundSql(String sqlText, List<ParameterMapping> mappingList) {
        SqlText = sqlText;
        this.mappingList = mappingList;
    }

    public String getSqlText() {
        return SqlText;
    }

    public void setSqlText(String sqlText) {
        SqlText = sqlText;
    }

    public List<ParameterMapping> getMappingList() {
        return mappingList;
    }

    public void setMappingList(List<ParameterMapping> mappingList) {
        this.mappingList = mappingList;
    }
}
