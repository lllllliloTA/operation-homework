package com.study.config;

import com.study.pojo.Configuration;
import com.study.pojo.MappedStatement;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class XMLMapperBuilder {

    private Configuration configuration;

    public XMLMapperBuilder(Configuration configuration) {
        this.configuration = configuration;
    }
    public void parse(InputStream in) throws DocumentException {

        Document d = new SAXReader().read(in);
        Element rootElement = d.getRootElement();

        List<Element> insertList = rootElement.selectNodes("//insert");
        List<Element> updateList = rootElement.selectNodes("//update");
        List<Element> deleteList = rootElement.selectNodes("//delete");
        List<Element> selectlist = rootElement.selectNodes("//select");
        String nameSpace = rootElement.attributeValue("namespace");

        selectlist.addAll(insertList);
        selectlist.addAll(updateList);
        selectlist.addAll(deleteList);

        for (Element  element  : selectlist) {
            //获取UserMapper.xml中 标签中的信息
            //获取标签的ID属性
            String id = element.attributeValue("id");
            //获取标签的resultType属性
            String resultType = element.attributeValue("resultType");
            //获取标签的paramterType属性
            String paramterType = element.attributeValue("paramterType");
            //获取标签的文本信息textTrim
            String textTrim = element.getTextTrim();
            //封装对象参数
            MappedStatement mappedStatement = new MappedStatement();
            mappedStatement.setId(id);
            mappedStatement.setResultType(resultType);
            mappedStatement.setParamterType(paramterType);
            mappedStatement.setSql(textTrim);
            //key是mapper.xml的唯一命名空间名字 : statementId由 namespace+"."+id组成
            String key = nameSpace + "." + id;
            configuration.getMappedStatementList().put(key,mappedStatement);

        }


    }

}
