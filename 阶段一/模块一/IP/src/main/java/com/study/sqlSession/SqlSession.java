package com.study.sqlSession;

import java.util.List;

public interface SqlSession  {

    //查询所有
    public <E> List<E> selectList(String statementId,Object... params) throws Exception;

    //查询单个
    public <T> T selectOne(String statementId, Object... params) throws Exception;

    //新增用户
    public Integer insertUser(String statementId,Object... params)throws Exception;

    //更新用户
    public Integer updateUser(String statementId,Object... params)throws Exception;

    //删除用户
    public Integer deleteUser(String statementId,Object... params)throws Exception;


    //为Dao层生成代理实现类
    public <T> T getMapper(Class<?> mapperClass);
}
