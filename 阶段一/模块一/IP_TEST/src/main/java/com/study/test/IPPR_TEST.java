package com.study.test;

import com.study.dao.IUserDao;
import com.study.io.Resources;
import com.study.pojo.User;
import com.study.sqlSession.SqlSession;
import com.study.sqlSession.SqlSessionFactory;
import com.study.sqlSession.SqlSessionFactoryBuilder;
import org.junit.Before;
import org.junit.Test;

import java.io.InputStream;
import java.util.List;

public class IPPR_TEST {

    private SqlSession sqlSession;

    @Before
    public void before() throws Exception {
        //第一步:通过传入的xml路径,将该路径的xml读取成字节输入流
        InputStream resourceAsSteam = Resources.getResourceAsSteam("sqlMapConfig.xml");
        //将InputSteam传递进持久层框架进行解析
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().builder(resourceAsSteam);

        sqlSession = sqlSessionFactory.openSession();
    }
    /**
     * 查询
     * */
    @Test
    public void test1() throws Exception{

        //产生JDK动态代理，为该接口生成代理对象
        IUserDao userDao = sqlSession.getMapper(IUserDao.class);
        //代理对象调用接口中的任意方法 都会去执行invoke里面的方法
        List<User> all = userDao.findAll();
        for (User user1 : all){
            System.out.println(user1);
        }
    }

    /**
     * 新增用户
     * */
    @Test
    public void test2() throws Exception{

        //产生JDK动态代理，为该接口生成代理对象
        IUserDao userDao = sqlSession.getMapper(IUserDao.class);
        //代理对象调用接口中的任意方法 都会去执行invoke里面的方法
        User user = new User();
        user.setId(5);
        user.setUsername("shenjiayi");
        user.setPassword("shenjiayi123");
        user.setBirthday("2021-02-02");

        Integer integer = userDao.insertUser(user);

        System.out.println(integer);
    }

    /**
     * 更新用户
     * */
    @Test
    public void test3() throws Exception{

        //产生JDK动态代理，为该接口生成代理对象
        IUserDao userDao = sqlSession.getMapper(IUserDao.class);
        //代理对象调用接口中的任意方法 都会去执行invoke里面的方法
        User user = new User();
        user.setId(5);
        user.setUsername("修改shenjiayi");

        Integer integer = userDao.updateUser(user);

        System.out.println(integer);
    }

    /**
     * 删除用户
     * */
    @Test
    public void test4() throws Exception{

        //产生JDK动态代理，为该接口生成代理对象
        IUserDao userDao = sqlSession.getMapper(IUserDao.class);
        //代理对象调用接口中的任意方法 都会去执行invoke里面的方法

        Integer integer = userDao.deleteUser(5);

        System.out.println(integer);
    }

}
