package com.homework.config;

import com.homework.servlet.MyServlet;
import org.apache.catalina.Context;
import org.apache.catalina.LifecycleException;
import org.apache.catalina.WebResourceRoot;
import org.apache.catalina.core.AprLifecycleListener;
import org.apache.catalina.core.StandardContext;
import org.apache.catalina.core.StandardServer;
import org.apache.catalina.startup.Tomcat;
import org.apache.catalina.webresources.DirResourceSet;
import org.apache.catalina.webresources.StandardRoot;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.DispatcherServlet;

import java.io.File;


public class SpringApplication {
    // 2. 编写run 方法 实现tomcat 启动
    public static void run(){
        try {
            Tomcat tomcat = new Tomcat();
            // 获取当前项目的绝对路劲
            StandardContext ctx = (StandardContext) tomcat.addWebapp("/",new File("src/main").getAbsolutePath());
            // 禁止重新载入
            ctx.setReloadable(false);
            // class文件地址
            File file = new File("target/classes");
            // 创建webRoot
            WebResourceRoot resource = new StandardRoot(ctx);
            // 让Tomcat内部读取class执行
            resource.addJarResources(new DirResourceSet(resource,"/WEB-INF/classes",file.getAbsolutePath(),"/"));
            tomcat.start();
            //异步调用等待
            tomcat.getServer().await();

        }catch (Exception e){
            e.printStackTrace();
        }

    }
    
    
    public int[] sort(){
        
        int[] a = {1,3,4,9};
        int[] b = {2,5,6};
        int[] c = null;

        int total = a.length+b.length;

        for (int i = 0 ; i <a.length ;i++) {
            int k = a[i];
            int size = 0;
            for (int j =0;j<b.length;j++){
                if(k<b[j]){

                }
            }

        }
        
        return null;
    }

}
