package com.homework.web;

import com.homework.SpringConfig;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
import javax.servlet.annotation.HandlesTypes;


public class MyWebApplicationInitializer implements WebApplicationInitializer {
    //4. 编写MyWebApplicationInitializer 并 实现WebApplicationInitializer接口 重写onStartup 方法
    //这个 onStartup 方法 会在tomcat启动的时候进行回调
    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        //通过注解的方式初始化Spring上下文
        AnnotationConfigWebApplicationContext anno = new AnnotationConfigWebApplicationContext();
        anno.register(SpringConfig.class);
        anno.refresh();

        //基于java代码的方式初始化DispatchServlet
        DispatcherServlet dispatcherServlet = new DispatcherServlet(anno);
        ServletRegistration.Dynamic app = servletContext.addServlet("dispatcher", dispatcherServlet);
        app.setLoadOnStartup(1);
        app.addMapping("/");


    }

}
