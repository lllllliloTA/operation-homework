package com.homework;

import com.homework.config.SpringApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
//3.创建配置类 扫描 包路径
@Configuration
@ComponentScan(basePackages = "com.homework")
public class SpringConfig {

    public static void main(String[] args) {
        SpringApplication.run();
    }

}
