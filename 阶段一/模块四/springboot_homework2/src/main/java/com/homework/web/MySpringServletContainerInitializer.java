package com.homework.web;

import org.springframework.web.WebApplicationInitializer;

import javax.servlet.ServletContainerInitializer;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.HandlesTypes;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

@HandlesTypes(MyWebApplicationInitializer.class)
public class MySpringServletContainerInitializer implements ServletContainerInitializer {
    // 5. 编写 MySpringServletContainerInitializer 方法 实现 ServletContainerInitializer 重写 onStartup
    //这个类会在tomcat 启动的过程中 通过Servlet3.0 会在tomcat加载的过程中
    // 通过SPI 将META-INF/services/javax.servlet.ServletContainerInitialier 加载里面的配置
    //6. 编写META-INF/services/javax.servlet.ServletContainerInitialier 文件
    @Override
    public void onStartup(Set<Class<?>> set, ServletContext servletContext) throws ServletException {

        List<WebApplicationInitializer> initializers = new LinkedList();
        Iterator setClass;
        if (set!=null){
            setClass = set.iterator();
            while (setClass.hasNext()){
                Class<?> nextClass = (Class<?>) setClass.next();

                if (!nextClass.isInterface()){
                    try {
                        initializers.add ((WebApplicationInitializer)nextClass.newInstance());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        if (!initializers.isEmpty()){
            setClass = initializers.iterator();

            while (setClass.hasNext()){
                WebApplicationInitializer nextInitializer = (WebApplicationInitializer)setClass.next();
                nextInitializer.onStartup(servletContext);
            }
        }
    }
}
